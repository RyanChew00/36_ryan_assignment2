﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {

        Game game;

        [SetUp]
        public void SetUp()
        {
             game = new Game();
        }

        public void Roll(int rolls)
        {
            for (int i = rolls; i < rolls; i++) ;
            {
                game.Roll(rolls);
            }
        }
   
        [Test]
        public void RollGutterGame()
        {
            game.Roll(0);
           
       
            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test]
        public void RollOnes()
        {
            game.Roll(1);
          
      
            Assert.That(game.Score(), Is.EqualTo(1));
        }
    }
}
